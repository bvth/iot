package IoTticket.Model;


import javax.persistence.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@Entity
@Table(name="value_table")
public class Torque
{
    @Id
    private int no;

    private String id;

    private String value;

    private String timestamp;

    public Torque() {
    }

    public Torque(int no, String id, String value, String timestamp) {
        this.no = no;
        this.id = id;
        this.value = value;
        this.timestamp = timestamp;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public Double getValueDouble(){
        StringBuilder sb = new StringBuilder(this.value);
        sb.insert(1,".");
        if(this.value.contains("+"))
            sb.insert(3,"E");
        else if(this.value.contains("."))
            sb.deleteCharAt(this.value.length());
        else
            sb.insert(3,"E-");
        return Double.parseDouble(sb.toString());
    }

    public String getTimestamp() {
        return timestamp;
    }

    public long getTimestampLong(){
        return Timestamp.valueOf(this.timestamp).getTime();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "#: "+this.no+"ID: "+this.getId()+" Value: "+this.getValue()+" Timestamp: "+this.getTimestamp();
    }
}
