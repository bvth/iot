package IoTticket;

import IoTticket.Model.Torque;
import com.iotticket.api.v1.IOTAPIClient;
import com.iotticket.api.v1.exception.ValidAPIParamException;
import com.iotticket.api.v1.model.*;
import com.iotticket.api.v1.model.Datanode.DatanodeRead;
import com.iotticket.api.v1.model.Datanode.DatanodeReadValue;
import com.iotticket.api.v1.model.Datanode.DatanodeWriteValue;

import java.sql.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;


public class Application{
    private final static String userName = "thanh.bui1995@gmail.com";
    private final static String pwd = "Buivanthanh95";
    private final static IOTAPIClient client = new IOTAPIClient("https://my.iot-ticket.com/api/v1", userName, pwd);

    private final static Logger log = Logger.getLogger(Application.class.getName());

    final static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public static void main(String[] args) throws ValidAPIParamException, SQLException, ClassNotFoundException, ParseException, InterruptedException {
//        SpringApplication.run(Application.class, args);
        String deviceId = "5188eaa129244a2cb4f4b27eba4e0566";
        boolean fromBeginning = false;

//        int tsOffset = 60245;
/*
// --------CREATE NEW DEVICE-------------
        Device d = new Device();
        d.setName("ABB IRB 1600");
        d.setManufacturer("ABB");
        d.setType("Industrial");

        Device.DeviceDetails deviceDetails = null;
        deviceDetails = client.registerDevice(d);
        deviceId = deviceDetails.getDeviceId();
        System.out.println("This is the device id that should be used in subsequent calls when sending measurement data: " + deviceDetails.getDeviceId());
*/
//----------------PRELOAD DATABASE----------------
        // Create a variable for the connection string.
        String connectionUrl = "jdbc:sqlserver://localhost;databaseName=Torque;user=admin;password=admin";

        // Declare the JDBC objects.
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        // Establish the connection.
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        con = DriverManager.getConnection(connectionUrl);

        // Create and execute an SQL statement that returns some data.
        stmt = con.createStatement();

        while(true) {
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            long tsIot = cal.getTimeInMillis();
            //CHECK FOR EXISTENCE OF DATANODES
            try {
                //GET CURRENT TIMESTAMP ON IOT
                for (int i = 1; i <= 6; i++) {
                    DatanodeQueryCriteria max = new DatanodeQueryCriteria(deviceId, "Max" + i);
                    DatanodeQueryCriteria min = new DatanodeQueryCriteria(deviceId, "Min" + i);
                    DatanodeQueryCriteria avg = new DatanodeQueryCriteria(deviceId, "Avg" + i);

                    max.setLimit(1);
                    max.setSortOrder(DatanodeQueryCriteria.Order.Descending);
                    min.setLimit(1);
                    min.setSortOrder(DatanodeQueryCriteria.Order.Descending);
                    avg.setLimit(1);
                    avg.setSortOrder(DatanodeQueryCriteria.Order.Descending);


                    ProcessValues maxValues = client.readProcessData(max);
                    ProcessValues minValues = client.readProcessData(min);
                    ProcessValues avgValues = client.readProcessData(avg);
                    Collection<DatanodeRead> maxReads = maxValues.getDatanodeReads();
                    Collection<DatanodeRead> minReads = minValues.getDatanodeReads();
                    Collection<DatanodeRead> avgReads = avgValues.getDatanodeReads();

                    //COMPARE IOT TIMESTAMP WITH CURRENT TIMESTAMP
                    tsIot = compareTimestamp(maxReads, tsIot);
                    tsIot = compareTimestamp(minReads, tsIot);
                    tsIot = compareTimestamp(avgReads, tsIot);
                }
            }
            catch (Exception e){
                System.out.println("Datanode does not exist!");
                //GET TIMESTAMP FROM THE BEGINNING
                tsIot = Timestamp.valueOf(findTimestamp("SELECT min(timestamp) FROM torque_value.value_table",stmt,rs)).getTime();
                fromBeginning = true;
            }

            //GET LATEST TIMESTAMP FROM DATABASE
            String lastTsDb = findTimestamp("SELECT max(timestamp) FROM torque_value.value_table",stmt,rs);
            long lastTsDbLong = Timestamp.valueOf(lastTsDb).getTime();

            //CHECK IF IOT DATA IS UP-TO-DATE
            List<Torque> torques = null;
            if (tsIot < lastTsDbLong) {
                log.info("IoT data needs update");
                //GET DATA AT THE TIME OF IOT
                if(fromBeginning)
                    torques = findDataByDate("select * from torque_value.value_table where timestamp >='"+df.format(tsIot)+"'", stmt, rs);
                else
                    torques = findDataByDate("select * from torque_value.value_table where timestamp >'"+df.format(tsIot)+"'", stmt, rs);
                fromBeginning=false;
            } else if (tsIot == lastTsDbLong)
                log.info("IoT data is Up-to-date");
            else
                log.warning("error");
//            torques = findDataByDate("select * from torque_value.value_table ", stmt, rs);

            //WRITING DATA
            //IF IOT IS NOT UP-TO-DATE
            if (torques != null) {
                int axis_no = 0;
                for (int i = 0; i < torques.size()/3; i++) {
                    updateProgress((double)i/(torques.size()/3));
//-------------SEND DATA -----------------
                    DatanodeWriteValue dnMax = new DatanodeWriteValue();
                    DatanodeWriteValue dnMin = new DatanodeWriteValue();
                    DatanodeWriteValue dnAvg = new DatanodeWriteValue();

                    dnMax.setDataType(DataType.DoubleType);
                    dnMin.setDataType(DataType.DoubleType);
                    dnAvg.setDataType(DataType.DoubleType);

                    if(axis_no==6) axis_no=0;

                    dnMax.setName("Max" + (axis_no + 1));
                    dnMin.setName("Min" + (axis_no + 1));
                    dnAvg.setName("Avg" + (axis_no + 1));
                    axis_no++;

                    Torque t1 = torques.get(i * 3 );
                    Torque t2 = torques.get(i * 3 + 1);
                    Torque t3 = torques.get(i * 3 + 2);

                    dnMax.setValue(t1.getValueDouble());
                    dnMax.setTimestampMiliseconds(t1.getTimestampLong());
                    WriteDataResponse writeResult = client.writeData(deviceId, dnMax);

                    dnMin.setValue(t2.getValueDouble());
                    dnMin.setTimestampMiliseconds(t2.getTimestampLong());
                    writeResult = client.writeData(deviceId, dnMin);

                    dnAvg.setValue(t3.getValueDouble());
                    dnAvg.setTimestampMiliseconds(t3.getTimestampLong());
                    writeResult = client.writeData(deviceId, dnAvg);
                }
                updateProgress(1.0);
                System.out.println();
                log.info("done updating");
            }
            TimeUnit.MINUTES.sleep(1);
        }
    }

    public static String findTimestamp(String query, Statement stmt, ResultSet rs) throws SQLException, ParseException {
        rs = stmt.executeQuery(query);
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnsNumber = rsmd.getColumnCount();
        String columnValue = "";
        while (rs.next()) {
            for (int i = 1; i <= columnsNumber; i++) {
                if (i > 1) System.out.print(",  ");
                columnValue = rs.getString(i);
            }
        }
        stmt.clearBatch();
        stmt.clearWarnings();
        return columnValue;
    }

    public static long compareTimestamp(Collection<DatanodeRead> dn,long current_ts){
        for (DatanodeRead datanodeRead : dn) {
            for (DatanodeReadValue value : datanodeRead.getDatanodeReadValues()) {
                if (value.getTimestampMilliSecond() < current_ts)
                    current_ts = value.getTimestampMilliSecond();
            }
        }
        return current_ts;
    }

    public static List<Torque> findDataByDate(String query,Statement stmt, ResultSet rs) throws SQLException {
        rs = stmt.executeQuery(query);
        ResultSetMetaData rsmd = rs.getMetaData();
        List<Torque> torques = new ArrayList<Torque>();
        while (rs.next()) {
            torques.add(new Torque(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4)));
        }
        stmt.clearBatch();
        stmt.clearWarnings();
        return torques;
    }

    public static void sendDataIoT(DatanodeWriteValue dn, long ts,Double value,String deviceId) throws ValidAPIParamException {
            dn.setValue(value);
            dn.setTimestampMiliseconds(ts);
            WriteDataResponse writeResult = client.writeData(deviceId, dn);
    }
    static void updateProgress(double progressPercentage) {
        final int width = 50; // progress bar width in chars

        System.out.print("\rUpdating [");
        int i = 0;
        for (; i <= (int)(progressPercentage*width); i++) {
            System.out.print("=");
        }
        for (; i < (width); i++) {
            System.out.print(" ");
        }
        System.out.print("]"+String.format("%.1f",progressPercentage*100)+"%");
    }
}
